# Project description
This is a continuation of the project for [pizzeria](https://gitlab.com/pskrajnyy/pizzeria-project). This section implements functionality for pizzeria staff.

# Project objective
The main purpose of this project was to play a little bit with:
-React
-Redux
-Router

# Development

##URL's available by 'yarn start'

- http://localhost:3000 - administration panel for staff ('webpack-dev-server')
- http://localhost:3131 - pizzeria page for clients
- http://localhost:3131/api - Api project
- http://localhost:3131/api/db - database API
- http://localhost:3131/panel - status of the current project (adm. panel) from the moment of launching 'yarn start' (the page at this address will not change * live * along with changes made to the project files)
