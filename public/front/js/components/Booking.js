import { select, templates, settings, classNames } from '../settings.js';
import utils from '../utils.js';
import AmountWidget from './AmountWidget.js';
import DatePicker from './DatePicker.js';
import HourPicker from './HourPicker.js';

class Booking {
  constructor(element) {
    const thisBooking = this;

    thisBooking.render(element);
    thisBooking.initWidgets();
    thisBooking.getData();
    thisBooking.initAction();
  }

  render(element) {
    const thisBooking = this;
    const generatedHTML = templates.bookingWidget();

    thisBooking.dom = {};
    thisBooking.dom.wrapper = element;
    thisBooking.generatedDom = utils.createDOMFromHTML(generatedHTML);
    thisBooking.dom.wrapper.appendChild(thisBooking.generatedDom);
    thisBooking.dom.peopleAmount = thisBooking.generatedDom.querySelector(select.booking.peopleAmount);
    thisBooking.dom.hoursAmount = thisBooking.generatedDom.querySelector(select.booking.hoursAmount);
    thisBooking.dom.datePicker = thisBooking.generatedDom.querySelector(select.widgets.datePicker.wrapper);
    thisBooking.dom.hourPicker = thisBooking.generatedDom.querySelector(select.widgets.hourPicker.wrapper);
    thisBooking.dom.tables = thisBooking.dom.wrapper.querySelectorAll(select.booking.tables);
    thisBooking.dom.address = thisBooking.generatedDom.querySelector(select.booking.address);
    thisBooking.dom.phone = thisBooking.generatedDom.querySelector(select.booking.phone);
    thisBooking.dom.starters = thisBooking.generatedDom.querySelectorAll(select.booking.starter);
    thisBooking.dom.timePicker = thisBooking.generatedDom.querySelector(select.booking.timePicker);
  }

  initWidgets() {
    const thisBooking = this;

    thisBooking.peopleAmount = new AmountWidget(thisBooking.dom.peopleAmount);
    thisBooking.hoursAmount = new AmountWidget(thisBooking.dom.hoursAmount);
    thisBooking.datePicker = new DatePicker(thisBooking.dom.datePicker);
    thisBooking.hourPicker = new HourPicker(thisBooking.dom.hourPicker);

    thisBooking.dom.timePicker.addEventListener('updated', function() {
      thisBooking.updateDom();
    });

    thisBooking.dom.wrapper.addEventListener('submit', function() {
      event.preventDefault();

      if(thisBooking.validForm()) {
        thisBooking.sendBooking();
      }
    });
  }

  initAction() {
    const thisBooking = this;

    for(let table of thisBooking.dom.tables) {
      let tableId = parseInt(table.getAttribute(settings.booking.tableIdAttribute));
      table.addEventListener('click', function() {
        const choosenTable = table.classList.contains(classNames.booking.tableBooked);

        if(!choosenTable && thisBooking.clickedTable == null) {
          table.classList.add(classNames.booking.tableBooked);
          thisBooking.clickedTable = tableId;
        } else if(!table.available) {
          alert('Table is not available');
        } else {
          const oldTable = document.querySelectorAll('['+settings.booking.tableIdAttribute + '="' + thisBooking.clickedTable +'"]')[0];
          oldTable.classList.remove(classNames.booking.tableBooked);
          table.classList.add(classNames.booking.tableBooked);
          thisBooking.clickedTable = tableId;
        }
      });
    }
  }

  getData() {
    const thisBooking = this;
    const startDateParam = settings.db.dateStartParamKey + '=' + utils.dateToStr(thisBooking.datePicker.minDate);
    const endDateParam = settings.db.dateEndParamKey + '=' + utils.dateToStr(thisBooking.datePicker.maxDate);

    const params = {
      booking: [
        startDateParam,
        endDateParam,
      ],
      eventsCurrent: [
        settings.db.notRepeatParam,
        startDateParam,
        endDateParam,
      ],
      eventsRepeat: [
        settings.db.repeatParam,
        endDateParam,
      ],
    };

    const urls = {
      booking: settings.db.url +'/' + settings.db.booking + '?' + params.booking.join('&'),
      eventsCurrent: settings.db.url + '/' + settings.db.event + '?' + params.eventsCurrent.join('&'),
      eventsRepeat: settings.db.url + '/' + settings.db.event + '?' + params.eventsRepeat.join('&'),
    };

    Promise.all([
      fetch(urls.booking),
      fetch(urls.eventsCurrent),
      fetch(urls.eventsRepeat),
    ])
      .then(function(allResponses) {
        const bookingResponse = allResponses[0];
        const eventsCurrentResponse = allResponses[1];
        const eventsRepeatResponse = allResponses[2];
        return Promise.all([
          bookingResponse.json(),
          eventsCurrentResponse.json(),
          eventsRepeatResponse.json(),
        ]);
      })
      .then(function([bookings, eventsCurrent, eventsRepeat]) {
        thisBooking.parseData(bookings, eventsCurrent, eventsRepeat);
      });
  }

  parseData(bookings, eventsCurrent, eventsRepeat) {
    const thisBooking = this;
    const minDate = thisBooking.datePicker.minDate;
    const maxDate = thisBooking.datePicker.maxDate;

    thisBooking.booked = {};

    for(const item of eventsCurrent) {
      thisBooking.makeBooked(item.date, item.hour, item.duration, item.table);
    }

    for(const item of bookings) {
      thisBooking.makeBooked(item.date, item.hour, item.duration, item.table);
    }

    for(const item of eventsRepeat) {
      if(item.repeat == 'daily') {
        for(let loopDate = minDate; loopDate <= maxDate; loopDate = utils.addDays(loopDate, 1)) {
          thisBooking.makeBooked(utils.dateToStr(loopDate), item.hour, item.duration, item.table);
        }
      }
    }

    thisBooking.updateDom();
  }

  updateDom() {
    const thisBooking = this;

    thisBooking.date = thisBooking.datePicker.value;
    thisBooking.hour = utils.hourToNumber(thisBooking.hourPicker.value);
    thisBooking.clickedTable = null;

    let allAvailable = false;

    if(typeof thisBooking.booked[thisBooking.date] == 'undefined' || typeof thisBooking.booked[thisBooking.date][thisBooking.hour] == 'undefined') {
      allAvailable = true;
    }

    for(let table of thisBooking.dom.tables) {
      let tableId = table.getAttribute(settings.booking.tableIdAttribute);
      if(!isNaN(tableId)) {
        tableId = parseInt(tableId);
      }

      if(!allAvailable && thisBooking.booked[thisBooking.date][thisBooking.hour].includes(tableId)) {
        table.classList.add(classNames.booking.tableBooked);
        table.available = false;
      } else {
        table.classList.remove(classNames.booking.tableBooked);
        table.available = true;
      }
    }

    thisBooking.rangeSliderColour();
  }

  makeBooked(date, hour, duration, table) {
    const thisBooking = this;

    if(typeof thisBooking.booked[date] == 'undefined') {
      thisBooking.booked[date] = {};
    }

    const startHour = utils.hourToNumber(hour);

    for( let hourBlock = startHour; hourBlock < startHour + duration; hourBlock += 0.5){
      if(typeof thisBooking.booked[date][hourBlock] == 'undefined') {
        thisBooking.booked[date][hourBlock] = [];
      }

      thisBooking.booked[date][hourBlock].push(table);
    }
  }

  validForm() {
    const thisBooking = this;
    const table = thisBooking.clickedTable;
    const address = thisBooking.dom.address.value;
    const phone = thisBooking.dom.phone.value;
    const numbers = /^[0-9]+$/;

    if( table == null || address == '' || phone.length > 9 || !phone.match(numbers)) {
      alert('Please write correct value in form');
      return false;
    }

    return true;
  }

  sendBooking() {
    const thisBooking = this;
    const url = settings.db.url + '/' + settings.db.booking;

    const bookingData = {
      date: thisBooking.date,
      hour: utils.numberToHour(thisBooking.hour),
      table: thisBooking.clickedTable,
      duration: thisBooking.hoursAmount.value,
      ppl: thisBooking.peopleAmount.value,
      address: thisBooking.dom.address.value,
      phone: thisBooking.dom.phone.value,
      starters: []
    };

    for(const starter of thisBooking.dom.starters) {
      if(starter.checked) {
        bookingData.starters.push(starter.value);
      }
    }

    const options = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(bookingData),
    };

    fetch(url, options)
      .then(function(response) {
        return response.json();
      }).then(function() {
        thisBooking.makeBooked(bookingData.date, bookingData.hour, bookingData.duration, bookingData.table);
        thisBooking.getData();
        thisBooking.resetToDefault();
        alert('Book success');
      });
  }

  resetToDefault() {
    const thisBooking = this;

    thisBooking.peopleAmount.value = 1;
    thisBooking.hoursAmount.value = 1;
    thisBooking.dom.address.value = null;
    thisBooking.dom.phone.value = null;

    for(const starter of thisBooking.dom.starters) {
      starter.checked = false;
    }
    thisBooking.updateDom();
  }

  rangeSliderColour() {
    const thisBooking = this;
    const bookedHours = thisBooking.booked[thisBooking.date];
    const sliderColours = [];
    thisBooking.dom.rangeSlider = thisBooking.dom.wrapper.querySelector('.rangeSlider');
    const slider = thisBooking.dom.rangeSlider;

    for (var number = 12; number <= 24; number = number + 0.5) {
      const firstOfInterval = ((number - 12) * 100) / 12;
      const secondOfInterval = (((number - 12) + .5) * 100) / 12;

      if(!bookedHours.hasOwnProperty(number)) {
        sliderColours.push('/*' + number + '*/green ' + firstOfInterval + '%, green ' + secondOfInterval + '%');
      }
    }

    for (let bookedHour in bookedHours) {
      const firstOfInterval = ((bookedHour - 12) * 100) / 12;
      const secondOfInterval = (((bookedHour - 12) + .5) * 100) / 12;

      if (bookedHours[bookedHour].length <= 1) {
        sliderColours.push('/*' + bookedHour + '*/green ' + firstOfInterval + '%, green ' + secondOfInterval + '%');
      } else if (bookedHours[bookedHour].length === 2) {
        sliderColours.push('/*' + bookedHour + '*/orange ' + firstOfInterval + '%, orange ' + secondOfInterval + '%');
      } else if (bookedHours[bookedHour].length === 3) {
        sliderColours.push('/*' + bookedHour + '*/red ' + firstOfInterval + '%, red ' + secondOfInterval + '%');
      }
    }

    sliderColours.sort();
    const liveColours = sliderColours.join();
    slider.style.background = 'linear-gradient(to right, ' + liveColours + ')';

    const sliderElement = document.getElementsByClassName('rangeSlider__fill__horizontal');
    sliderElement[0].classList.remove('rangeSlider__fill__horizontal');
  }
}

export default Booking;
