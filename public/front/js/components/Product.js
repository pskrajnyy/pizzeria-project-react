import { templates, classNames, select } from '../settings.js';
import utils from '../utils.js';
import AmountWidget from './AmountWidget.js';

class Product {
  constructor(id, data) {
    const thisProduct = this;

    thisProduct.id = id;
    thisProduct.data = data;

    thisProduct.renderInMenu();
    thisProduct.getElement();
    thisProduct.initAccordion();
    thisProduct.initOrderForm();
    thisProduct.initAmountWidget();
    thisProduct.initActions();
  }

  renderInMenu() {
    const thisProduct = this;
    const generatedHTML = templates.menuProduct(thisProduct.data);

    thisProduct.element = utils.createDOMFromHTML(generatedHTML);

    const menuContainer = document.querySelector(select.containerOf.menu);

    menuContainer.appendChild(thisProduct.element);
  }

  initActions() {
    const thisProduct = this;

    document.addEventListener('edit', function () {
      thisProduct.updateProduct(event.detail.cartProduct);
    });
  }

  getElement() {
    const thisProduct = this;

    thisProduct.accordionTrigger = thisProduct.element.querySelector(select.menuProduct.clickable);
    thisProduct.form = thisProduct.element.querySelector(select.menuProduct.form);
    thisProduct.formInputs = thisProduct.form.querySelectorAll(select.all.formInputs);
    thisProduct.cartButton = thisProduct.element.querySelector(select.menuProduct.cartButton);
    thisProduct.priceElem = thisProduct.element.querySelector(select.menuProduct.priceElem);
    thisProduct.imageWrapper = thisProduct.element.querySelector(select.menuProduct.imageWrapper);
    thisProduct.amountWidgetElem = thisProduct.element.querySelector(select.menuProduct.amountWidget);
  }

  initOrderForm() {
    const thisProduct = this;

    thisProduct.form.addEventListener('submit', function (event) {
      event.preventDefault();
      thisProduct.processOrder();
    });

    for (let input of thisProduct.formInputs) {
      input.addEventListener('change', function () {
        thisProduct.processOrder();
      });
    }

    thisProduct.cartButton.addEventListener('click', function (event) {
      event.preventDefault();
      thisProduct.processOrder();
      thisProduct.addToCart();
    });
  }

  processOrder() {
    const thisProduct = this;
    thisProduct.params = {};

    const formData = utils.serializeFormToObject(thisProduct.form);
    const productParams = thisProduct.data.params;
    let price = thisProduct.data.price;

    for (const paramId in productParams) {
      const productParam = productParams[paramId];
      for (const optionId in productParam.options) {
        const option = productParam.options[optionId];
        const optionSelected = formData.hasOwnProperty(paramId) && formData[paramId].indexOf(optionId) > -1;
        const allImagesOptionId = thisProduct.imageWrapper.querySelectorAll(`.${paramId}-${optionId}`);

        if (optionSelected && !option.default) {
          price += option.price;
        } else if (!optionSelected && option.default) {
          price -= option.price;
        }

        if (optionSelected) {
          if (!thisProduct.params[paramId]) {
            thisProduct.params[paramId] = {
              label: productParam.label,
              options: {},
            };
          }
          thisProduct.params[paramId].options[optionId] = option.label;

          for (let image of allImagesOptionId) {
            image.classList.add(classNames.menuProduct.imageVisible);
          }
        } else {
          for (let image of allImagesOptionId) {
            image.classList.remove(classNames.menuProduct.imageVisible);
          }
        }
      }
    }
    thisProduct.priceSingle = price;
    thisProduct.price = thisProduct.priceSingle * thisProduct.amountWidget.value;
    thisProduct.priceElem.innerHTML = thisProduct.price;
  }

  initAccordion() {
    const thisProduct = this;

    thisProduct.accordionTrigger.addEventListener('click', function () {
      const activeProducts = document.querySelectorAll(select.all.menuProductsActive);
      thisProduct.element.classList.add('active');
      thisProduct.processOrder();

      for (const activeProduct of activeProducts) {
        activeProduct.classList.remove('active');
      }
    });
  }

  initAmountWidget() {
    const thisProduct = this;

    thisProduct.amountWidget = new AmountWidget(thisProduct.amountWidgetElem);

    thisProduct.amountWidgetElem.addEventListener('updated', function () {
      thisProduct.processOrder();
    });
  }

  addToCart() {
    const thisProduct = this;
    thisProduct.name = thisProduct.data.name;
    thisProduct.amount = thisProduct.amountWidget.value;

    const event = new CustomEvent('add-to-cart', {
      bubbles: true,
      detail: {
        product: thisProduct,
      }
    });

    thisProduct.element.dispatchEvent(event);
  }

  updateProduct(cartProduct) {
    const thisProduct = this;
    const activeProducts = document.querySelectorAll(select.all.menuProductsActive);
    const productParams = cartProduct.params;

    if (cartProduct.name == thisProduct.data.name) {

      for (const activeProduct of activeProducts) {
        activeProduct.classList.remove('active');
      }

      thisProduct.element.classList.add('active');

      for (const element of thisProduct.form.elements) {
        element.checked = false;

        if(element.type == 'select-one') {
          element.value = productParams[element.name].options[Object.keys(productParams[element.name].options)];
        } else {
          if(productParams[element.name] && productParams[element.name].options[element.value]) {
            element.checked = true;
          }
        }
      }

      thisProduct.amountWidget.value = cartProduct.amount;
      thisProduct.priceElem.innerHTML = cartProduct.price;

      thisProduct.processOrder();
    }
  }
}

export default Product;
